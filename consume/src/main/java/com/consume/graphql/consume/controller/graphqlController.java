package com.consume.graphql.consume.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.catalina.WebResource;
import org.glassfish.jersey.client.ClientResponse;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;


@RestController
@RequestMapping("/graphql")
public class graphqlController {

    @GetMapping(value = "/1")
    public ResponseEntity<String> consume()  {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("content-type", "application/graphql"); // maintain graphql

        // query is a grapql query wrapped into a String
        String query1 = "{\n" +
                "    allAuthors {\n" +
                "       id\n" +
                "       name\n" +
                "    }\n" +
                "  }";

        String URL = "http://localhost:8085/graphql";

        return restTemplate.postForEntity(URL, new HttpEntity<>(query1, headers), String.class);
    }

    @GetMapping(value = "/2")
    public void consumeByJson()  {

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.add("content-type", "application/json"); // just modified graphql into json

        String query1 = "{\n" +
                "  \"query\": query {\n" +
                "    \"allAuthors\" {\n" +
                "          \"id\"\n" +
                "          \"name\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        String URL = "https://localhost:8085/graphql";

        ResponseEntity<String> response = restTemplate.postForEntity(URL, new HttpEntity<>(query1, headers), String.class);
        System.out.println("The response================="+response);
    }
}
